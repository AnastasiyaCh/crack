#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "md5.h"
#define SIZE 1000

int main(int argc, char *argv[]) {
    
    // check if file name was supplied
    if (argc < 2) {
        puts("ERROR: Must supply at least one file name.");
        exit(1);
    } 
    
    FILE *sourceFile = fopen(argv[1], "r");
    
    // check if source file available for reading
    if ( !sourceFile ) {
        printf("Can't open %s for reading.\n", argv[1]);
        exit(1);
    }
    
    FILE *destinationFile = fopen(argv[2], "w");
    if (!destinationFile){
        printf("Can't open %s for writing.\n", argv[2]);
        exit(1);
    }
    
    char line[SIZE];
    char *line1;
    
    while (fgets(line, SIZE, sourceFile) != 0) {
        
        // hashing extracted line
        line1 = md5(line, strlen(line) - 1);
        strcat(line1, "\n");
        
        // print hashes into destination file
        fprintf(destinationFile, "%s", line1);
        
        
    }
    
    free(line1);
    fclose(sourceFile);
    fclose(destinationFile);
}